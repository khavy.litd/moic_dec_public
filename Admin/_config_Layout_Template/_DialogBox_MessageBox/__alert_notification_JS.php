
<?php  // *********** ຕົວຢ່າງ ວິທີການ : ເອົາໄປນໍາໃຊ້ *************** 
/* 
__alert_notification_JS() 

<button onclick="_shared_FUNC_Call_Alert_notification('cart',  'Call from JS', 3)"> Show Notification</button>


<script>
$(document).ready(function(){ 
    _shared_FUNC_Call_Alert_notification('cart',  'Call from JS' , 3);      
  });
</script>



 */  ?>

<?php
function __alert_notification_JS()  {   
?>
<style>
#Khavy_el_Alert_Notif_Main_Shared { 
position: fixed; 
width: 100%; top: 0; z-index: 91197;
background-color: #ec322a;
color: white; 
display: none; /* on load First : hide */

/* Start the shake animation and make the animation last for 0.5 seconds */
  animation: shake 0.9s;
  /* When the animation is finished, start again */
  animation-iteration-count: 1; /* infinite; */
}
@keyframes shake {
  0% { transform: translate(1px, 1px) rotate(0deg); }
  10% { transform: translate(-1px, -2px) rotate(-1deg); }
  20% { transform: translate(-3px, 0px) rotate(1deg); }
  30% { transform: translate(3px, 2px) rotate(0deg); }
  40% { transform: translate(1px, -1px) rotate(1deg); }
  50% { transform: translate(-1px, 2px) rotate(-1deg); }
  60% { transform: translate(-3px, 1px) rotate(0deg); }
  70% { transform: translate(3px, 1px) rotate(-1deg); }
  80% { transform: translate(-1px, -1px) rotate(1deg); }
  90% { transform: translate(1px, 2px) rotate(0deg); }
  100% { transform: translate(1px, -2px) rotate(-1deg); }
}
</style>


<div id="Khavy_el_Alert_Notif_Main_Shared" class="alert "   onclick="document.getElementById('Khavy_el_Alert_Notif_Main_Shared').style.display = 'none'">
	<i id="Notif_type"  class="fa fa-bell"   style="font-size: 28px; color: white;  position:absolute;"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<strong id="Notif_text" >ສະແດງຂໍ້ຄວາມ.</strong>
	<button type="button" class="close" >
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<script>
//  _shared_FUNC_Call_Alert_notification("info", "Call from JS" , 3); // "primary"

var Global_Timer_Notifi;  // ປະກາດ Var ໄວ້ເປັນ Global ນອກ FUNCTION ເພື່ອ ປ້ອງກັນບັນຫາ Call Mulitiple Timer 

function _shared_FUNC_Call_Alert_notification(GET_type="primary", GET_text="" , GET_Timer_Display=3) { 
	document.getElementById("Khavy_el_Alert_Notif_Main_Shared").style.display = "none"; // "none"=hide 

	// "primary" // "info"  // "success" // "warning"  // "danger"
	var icon_header = "fa fa-bell";
	switch (GET_type) {
    	case "primary": icon_header = "fa fa-bell"; break;
        case "info": icon_header = "fa fa-info"; break;
        case "success": icon_header = "fa fa-check"; break;
        case "warning": icon_header = "fa fa-exclamation-circle"; break;
        case "danger": icon_header = "fa fa-question-circle"; break;
        default: icon_header = "fa fa-bell"; 
	}

	 var el_Notif_type = document.getElementById("Notif_type"); el_Notif_type.className = icon_header; // "fab fa-app-store"  += icon_header += ມັນຈະຊ້ອນກັນຂຶ້ນເລື່ຶອຍໆ
	// document.getElementById("Notif_type").innerHTML = icon_header;   // For : https://material.io/tools/icons/?icon=notifications_active&style=baseline
	document.getElementById("Notif_text").innerHTML = GET_text;  

	clearTimeout(Global_Timer_Notifi); // ປ້ອງກັນບັນຫາ Call Mulitiple Timer ສ້າງໃໝ່ຂຶ້ນຫຼາຍຕົວ ຊ້ອນກັນ
	Global_Timer_Notifi = setTimeout(function() { document.getElementById("Khavy_el_Alert_Notif_Main_Shared").style.display = "none";  },  GET_Timer_Display * 1000); // 3000

	document.getElementById("Khavy_el_Alert_Notif_Main_Shared").style.display = "block"; // "none"=hide , "block" = Show
}

</script>


<?php
}
?>




