

<?php /* 
// ໃຊ້ສໍາຫຼັບ Call ໃນ JS Funtion( JavaScript )

// ########## ຫ້າມ ບໍ່ບໍ່ບໍ່ ບໍ່ໃຫ້ໃສ່ ວົງເລັບFunc () ໃນ ຕອນສົ່ງ parameter"Get_Func" ('info', '', '', ObjJS.FUNC_JS() ) #############
// ########## ເພາະມັນຈະ Auto RUN "myFunc()" ທັນທີເລີຍ , ຕ້ອງໃສ່ແບບນີ້: onclick="( 'info', '', '', ObjJS.FUNC_JS )"
// : ຫ້າມ ບໍ່ບໍ່ບໍ່ ບໍ່ໃຫ້ໃຊ້ແບບນີ້ ມີບັນຫາ Add Event Functions ຊ້ອນກັນ Clear ບໍ່ບໍ່ ບໍ່ໄດ້ // document.getElementById("el_ID_btn_onClick_OK").addEventListener("click", GET_Function_Set_Btn_onClick_OK); 
// ໃຊ້ jQuery ແທນ ຮ່ວມກັບ Clear Function OnClick=""; $( "#el_ID_btn_onClick_OK" ).off( "click", null ); // Clear / Remove ALL Funtions on <Btn  OnClick="" > 

++ ວິທີ ເອີ້ນໃຊ້ໃນ/ຈາກ JavaScript 

__alert_dialogBox_confirm_Yes_No_JS();

<button onclick="_shared_FUNC_Call_Alert_dialogBox_confirm_Yes_No('info', 'ຂໍ້ຄວາມແຈ້ງ', 'Call from JS', Func_JS_AJAX_Add_to_Cart)"> JS Allert YES, No</button>
<button onclick="_shared_FUNC_Call_Alert_dialogBox_confirm_Yes_No('info', 'ຂໍ້ຄວາມແຈ້ງ', 'Call from JS', FUNC_Pure_JS_LAOs)"> PureJS Allert YES, No</button>


function Func_JS_AJAX_Add_to_Cart() {
	_shared_FUNC_Call_Alert_notification('info',  'Click Ok Add to Cart' , 3);  
} 

function FUNC_Pure_JS_LAOs() {
	alert('FUNC_Pure_JS External JavaScript'); 
} 

</script>



*/ ?>


<?php
function __alert_dialogBox_confirm_Yes_No_JS()  {
?>

<!-- Modal -->
<div class="modal fade" id="_ID_Modal_alert_dialogBox_confirm_Yes_No_JS" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" >
    <div class="modal-content" >
      <div class="modal-header ">
        <h5 class="modal-title">
        <i id="msgBox1_type" class="fa fa-bell" style="color:#ec322a;  font-size:50px;"></i> 
        <a id="msgBox1_title"></a></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body" id="msgBox1_text">ສະແດງ msgBox1_text</div>
        <div class="modal-footer">
   
        <button id="el_ID_btn_onClick_OK" type="button" class="btn btn-primary"  data-dismiss="modal">ok</button>
        
<!--  <button type="button" class="btn btn-primary" >ຕົກລົງ</button> -->
<!--   <button id="del1"  type="button" class="btn btn-warning" data-dismiss="modal">XXX</button> --> 
        <button type="button" class="btn " data-dismiss="modal">Cancel</button>
        
      </div>
    </div>
  </div>
</div>



<script>
// onclick="_shared_FUNC_Call_Alert_dialogBox_confirm_Yes_No('info', 'ຂໍ້ຄວາມແຈ້ງ', 'ຫ້າມ ບໍ່ ບໍ່ໃຫ້ໃສ່ ວົງເລັບFunc () ', Func_AJAX_Add_to_Cart)"


function _shared_FUNC_Call_Alert_dialogBox_confirm_Yes_No(GET_type="primary", GET_title="ຂໍ້ຄວາມແຈ້ງ", 
		GET_text="Confirm Yes NO from JS" , GET_Function_Set_Btn_onClick_OK=null) { 

	$( "#el_ID_btn_onClick_OK" ).off( "click", null ); // Clear / Remove ALL Funtions on <Btn  OnClick="" > 

	// "primary" // "info"  // "success" // "warning"  // "danger"
	var icon_header = "fa fa-bell";
	switch (GET_type) {
    	case "primary": icon_header = "fa fa-bell"; break;
        case "info": icon_header = "fa fa-info"; break;
        case "success": icon_header = "fa fa-check"; break;
        case "warning": icon_header = "fa fa-exclamation-circle"; break;
        case "danger": icon_header = "fa fa-question-circle"; break;
        default: icon_header = "fa fa-bell"; 
	}
	
	var el_msgBox_type = document.getElementById("msgBox1_type"); el_msgBox_type.className = icon_header; // "fab fa-app-store" += icon_header += ມັນຈະຊ້ອນກັນຂຶ້ນເລື່ຶອຍໆ
	// document.getElementById("msgBox1_type").innerHTML = icon_header;  // For : https://material.io/tools/icons/?icon=notifications_active&style=baseline 
	document.getElementById("msgBox1_title").innerHTML = GET_title; 
	document.getElementById("msgBox1_text").innerHTML = GET_text;  
	$( "#el_ID_btn_onClick_OK" ).off( "click", null ); // Clear / Remove ALL Funtions on <Btn  OnClick="" > 
	
	// Add External Funtion to <Btn  OnClick="" >
	// ########## ຫ້າມ ບໍ່ບໍ່ບໍ່ ບໍ່ໃຫ້ໃສ່ ວົງເລັບFunc () ໃນ ຕອນສົ່ງ parameter"Get_Func" ('info', '', '', ObjJS.FUNC_JS() ) #############
	// ########## ເພາະມັນຈະ Auto RUN "myFunc()" ທັນທີເລີຍ , ຕ້ອງໃສ່ແບບນີ້: onclick="( 'info', '', '', ObjJS.FUNC_JS )"
	// : ຫ້າມ ບໍ່ບໍ່ບໍ່ ບໍ່ໃຫ້ໃຊ້ແບບນີ້ ມີບັນຫາ Add Event Functions ຊ້ອນກັນ Clear ບໍ່ບໍ່ ບໍ່ໄດ້ // document.getElementById("el_ID_btn_onClick_OK").addEventListener("click", GET_Function_Set_Btn_onClick_OK); 
	// ໃຊ້ jQuery ແທນ ຮ່ວມກັບ Clear Function OnClick=""; $( "#el_ID_btn_onClick_OK" ).off( "click", null ); // Clear / Remove ALL Funtions on <Btn  OnClick="" > 

	// console.log(GET_Function_Set_Btn_onClick_OK.name); // Get Function Name
	  $( "#el_ID_btn_onClick_OK" ).off( "click", null ); // Clear / Remove ALL Funtions on <Btn  OnClick="" > 
	  $( "#el_ID_btn_onClick_OK" ).on( "click", GET_Function_Set_Btn_onClick_OK ); // // Add External Funtion to <Btn  OnClick="" >

	  $('#_ID_Modal_alert_dialogBox_confirm_Yes_No_JS').modal('show');
} // function

</script>


<?php
}
?>




<?php /* 



*/ ?>






