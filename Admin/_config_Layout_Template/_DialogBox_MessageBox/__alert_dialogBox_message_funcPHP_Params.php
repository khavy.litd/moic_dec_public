

<?php
// // ໃຊ້ສໍາຫຼັບ Call ໃນ PHP ທໍາມະດາທົ່ວໄປ 

//  ຕົວຢ່າງ ວິທີການ : ເອົາໄປນໍາໃຊ້ 
//       __alert_dialogBox_message_funcPHP_Params("success", "ແຈ້ງເຕືອນ", "ລົບຂໍ້ມູນສໍາເລັດ") ;

function __alert_dialogBox_message_funcPHP_Params($str_type, $str_Title, $str_Message)  { 
    // "primary" // "info"  // "success" // "warning"  // "danger" 
    $icon_header = "fa fa-bell";
    switch ($str_type) {
        case "primary": $icon_header = "fa fa-bell"; break;
        case "info": $icon_header = "fa fa-info"; break;
        case "success": $icon_header = "fa fa-check"; break;
        case "warning": $icon_header = "fa fa-exclamation-circle"; break;
        case "danger": $icon_header = "fa fa-question-circle"; break;
        default: $icon_header = "fa fa-bell"; 
            
    }
    
?>


<!-- Modal -->
<div class="modal fade" id="_ID_Modal_Alert_dialogBox_message_funcPHP_Params" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" >
    <div class="modal-content" >
      <div class="modal-header ">
        <h5 class="modal-title">
        <i class="<?php  echo $icon_header; ?>" style="color:#ec322a; font-size:30px;"></i> 
        <a><?php echo $str_Title; ?></a></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body" id="msgBox_text"><?php echo $str_Message; ?></div>
        <div class="modal-footer">
   <!--  <button type="button" class="btn btn-primary" >ຕົກລົງ</button> -->
        <button type="button" class="btn "  data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

       

<script> //  Auto Run Modal on Event Page Load 
  // Event On Page Load
  $(document).ready(function(){ $('#_ID_Modal_Alert_dialogBox_message_funcPHP_Params').modal('show'); });
</script>

<?php
}
?>




