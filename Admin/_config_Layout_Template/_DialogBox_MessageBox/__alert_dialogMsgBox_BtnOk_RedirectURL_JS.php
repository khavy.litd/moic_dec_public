

<?php /* 
// ໃຊ້ສໍາຫຼັບ Call ໃນ JS Funtion( JavaScript )
// ເມື່ອ onClick Button Ok ແລ້ວຈຶ່ງ Redirect to URL

++ ວິທີ ເອີ້ນໃຊ້ໃນ/ຈາກ JavaScript 

__alert_dialogBox_message_JS();
<button onClick="_shared_FUNC_Call_AlertMsgBox_BtnOk_RedirectURL('info', 'ຂໍ້ຄວາມແຈ້ງ', 'Call from JS', 'index.php')" > </button>


<script>
$(document).ready(function(){ 
    _shared_FUNC_Call_AlertMsgBox_BtnOk_RedirectURL("info", "ຂໍ້ຄວາມແຈ້ງ", "Call from JS", "index.php");    
  });
</script>


*/ ?>


<?php
function __alert_dialogMsgBox_BtnOk_RedirectURL_JS()  {
?>

<!-- Modal -->
<div class="modal fade" id="_ID_Modal_alert_dialogMsgBox_BtnOk_RedirectURL_JS" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" >
    <div class="modal-content" >
      <div class="modal-header ">
        <h5 class="modal-title">
        <i id="msgBox2_type" class="fa fa-bell" style="color:#ec322a; font-size:50px;"></i> 
        <a id="msgBox2_title"></a></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body" id="msgBox2_text">ສະແດງ msgBox2_text</div>
        <div class="modal-footer">
		<a id="msgBox2_LinkURL" href="" class="btn btn-primary" >Ok</a>
        <button type="button" class="btn " data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
//  _shared_FUNC_Call_AlertMsgBox_BtnOk_RedirectURL("info", "ຂໍ້ຄວາມແຈ້ງ", "Call from JS", "index.php"); 

function _shared_FUNC_Call_AlertMsgBox_BtnOk_RedirectURL(
		GET_type="primary", GET_title="ຂໍ້ຄວາມແຈ້ງ", GET_text="", Get_URL=null) { 

	// "primary" // "info"  // "success" // "warning"  // "danger"
	var icon_header = "fa fa-bell";
	switch (GET_type) {
    	case "primary": icon_header = "fa fa-bell"; break;
        case "info": icon_header = "fa fa-info"; break;
        case "success": icon_header = "fa fa-check"; break;
        case "warning": icon_header = "fa fa-exclamation-circle"; break;
        case "danger": icon_header = "fa fa-question-circle"; break;
        default: icon_header = "fa fa-bell"; 
	}
	
	 var el_msgBox_type = document.getElementById("msgBox2_type"); el_msgBox_type.className = icon_header; // "fab fa-app-store" ຖ້າໃສ່ += icon_header += ມັນຈະຊ້ອນກັນຂຶ້ນເລື່ຶອຍໆ
	// document.getElementById("msgBox2_type").innerHTML = icon_header;  // For : https://material.io/tools/icons/?icon=notifications_active&style=baseline 
	
	var el_msgBox_LinkURL = document.getElementById("msgBox2_LinkURL"); el_msgBox_LinkURL.href = Get_URL; 
	document.getElementById("msgBox2_title").innerHTML = GET_title; 
	document.getElementById("msgBox2_text").innerHTML = GET_text;  
	$('#_ID_Modal_alert_dialogMsgBox_BtnOk_RedirectURL_JS').modal('show');
}

</script>

<?php
}
?>



