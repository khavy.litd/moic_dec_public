
  <!--header-->
  <div id="Header">
    <div class="header">
      <div class="box-logo-user"> <img src="<?php echo GLOBAL_Path_icon_Template_image_Admin; ?>admin_home_logo2.png" alt=""> </div>
      <div class="box-user">
        <div class="box-user-name">
          <ul class="list-user">
            <li> <a href="<?php echo GLOBAL_FULL_URL_Path_dir_Project; ?>home.php" id="home1">
              <table cellspacing="0" cellpadding="0" border="0">
                <tbody>
                  <tr>
                    <td><i class="fas fa-home"></i></td>
                    <td class="hide-414">Home</td>
                  </tr>
                </tbody>
              </table>
              </a> </li>
            <li> </li>
            <li> <a href="#nogo">
              <table cellspacing="0" cellpadding="0" border="0">
                <tbody>
                  <tr>
                    <td><i class="fas fa-download"></i></td>
                    <td class="hide-414">Manual</td>
                  </tr>
                </tbody>
              </table>
              </a> </li>


            <li><a href="<?php echo GLOBAL_FULL_URL_Path_dir_Project; ?>101_login/login_profile.view.php">
              <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                  <tr>
                    <td><div class="box-user-img"><img src="<?php echo GLOBAL_Path_main_Folder_photo."Admin/users_admin/".$_SESSION["users_admin+photo_filename"]; ?>" onerror="this.src='<?php echo GLOBAL_Path_icon_Template_image_Admin; ?>users_admin_photo/no_photo.png';" alt=""> </div></td>
                    <td><h2><?php echo $_SESSION["users_admin+full_name"]; ?></h2></td>
                  </tr>
                </tbody>
              </table>
              </a>
            </li>

            <li> </li>
            <li><a href="<?php echo GLOBAL_FULL_URL_Path_dir_Project; ?>101_login/_logout.action.php">
              <table cellspacing="0" cellpadding="0" border="0">
                <tbody>
                  <tr>
                    <td><i class="fas fa-sign-out-alt"></i></td>
                    <td class="hide-414">Logout</td>
                  </tr>
                </tbody>
              </table>
              </a> </li>
          </ul>
        </div>
      </div>
    </div>
  </div><!--  <div id="Header"> -->
 <!--//header--> 
