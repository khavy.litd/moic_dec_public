<?php // ຕ້ອງວ່າງ <?php ໄວ້ Top ສຸດ file_Excel.xlsx ຈຶ່ງຖືກ Downloaded ບໍ່ບໍ່ມີ ERROR ຕອນ Open ອ່ານ 

// session_start(); // 

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if(isset($_GET["click_btn_link_export_to_excel"])){
  session_start(); // 
  //require_once("../../_config_Layout_Template/config.inc.php"); // ຫ້າມ include config ເຂົ້າ file_Excel.xlsx Downloaded ມັນຈະມີ ERROR ຕອນ Open ອ່ານ 
  require_once("../../_Extensions.Tool/PhpSpreadsheet/vendor/autoload.php");  
  _Func_Export_Excel_GET_Auto_Download();   
} 


function class20_HTML_JS_Button_Export_Data_Table_To_Excel($axios_post_URL="ReportData.view.php", $Set_file_name="report") { 
    echo '<a onclick="AJAX_Send_Data_Table_Export_To_Excel()" 
          href="#" style="background:#eca52a" class="btn-add"><i class="far fa-file-excel"></i> Export Report</a>';
?>
    <script> // AJAX 
        function AJAX_Send_Data_Table_Export_To_Excel() { 

            // AJAX 
            var params = new URLSearchParams();
            params.append("AJAX_Sent+||+ROOT_Path_config_inc" , "<?php  echo ROOT_Server_Directory_Path_Project."_config_Layout_Template/config.inc.php"; ?>");
            params.append("Req_Call_Func" , "FuncJsAJAX_Get_Select_fetchAll_for_Arr_export_to_excel_GET_ALL_dt_Lists");

            axios.post("<?php  echo $axios_post_URL; ?>" , params) // 
              .then(function(res) { //[ if Success ] Get Return JSON data from url .php : Data Response
                console.log(res.data);  // console.log(res.data.status);   //   console.log(res.data.message);

                window.open('<?php  echo GLOBAL_FULL_URL_Path_dir_Project."class20_shared_include_func/export_to_excel/class20_shared_export_to_Excel_AJAX_download.php?click_btn_link_export_to_excel=true&Set_file_name=".$Set_file_name; ?>', '_blank');

                  // var status = res.data.status;
                  // var message = res.data.message;
                  // if(status === "success") { 
                  // // _shared_FUNC_Call_Alert_dialogBox_Message("info", "ຂໍ້ຄວາມແຈ້ງ", "Export Successful");
                  // } 

              }).catch(function (error) {  console.log(error);  });
        }
    </script>
<?php 
} 
?>

<?php 

function _Func_Export_Excel_GET_Auto_Download() {  

  $_Arr_export_to_excel_Set_Col_header = $_SESSION["_Arr_export_to_excel_Set_Col_header"];  
  $_Arr_export_to_excel_GET_ALL_dt_Lists = $_SESSION["_Arr_export_to_excel_GET_ALL_dt_Lists"];  

  $Set_file_name = $_GET["Set_file_name"];

  if(!isset($_SESSION["_Arr_export_to_excel_GET_ALL_dt_Lists"])) { return; } 
  if($_SESSION["_Arr_export_to_excel_GET_ALL_dt_Lists"] == null) { return; }


 // Create new Spreadsheet object
  $spreadsheet = new Spreadsheet();


    foreach ($_Arr_export_to_excel_Set_Col_header as $setOn_Col_Row => $value) { 
      // Styles 
      $spreadsheet->getActiveSheet()->getStyle($setOn_Col_Row)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE);
      $spreadsheet->getActiveSheet()->getStyle($setOn_Col_Row)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('8FBC8F');
    } 
    


  /*
  //Set active sheet index to the first sheet, 
  //and add some data
  $spreadsheet->setActiveSheetIndex(0)
  ->setCellValue('A1', 'This')
  ->setCellValue('B2', 'is')
  ->setCellValue('C1', 'a')
  ->setCellValue('D2', 'test.');
  */

  /*
  foreach ($_Arr_export_to_excel_Set_Col_header as $setOn_Col_Row => $value) {
    $spreadsheet->getActiveSheet()->setCellValue($setOn_Col_Row, $value);
  } */ 


  /*
  $arrayData = [
    ["Col", 2010, 2011, 2012, "Col", "Field"],
  ]; */
  $spreadsheet->getActiveSheet()
  ->fromArray(
      $_Arr_export_to_excel_Set_Col_header,  // The data to set
      NULL,        // Array values with this value will not be set
      'A1'         // Top left coordinate of the worksheet range where
                   //    we want to set these values (default is A1)
  );


  /*
  $arrayData = [
    [NULL, 2010, 2011, 2012],
    ['Q1',   12,   15,   21],
    ['Q2',   56,   73,   86],
    ['Q3',   52,   61,   69],
    ['Q4',   30,   32,    0],
  ]; */
  $spreadsheet->getActiveSheet()
    ->fromArray(
        $_Arr_export_to_excel_GET_ALL_dt_Lists,  // The data to set
        NULL,        // Array values with this value will not be set
        'A2'         // Top left coordinate of the worksheet range where
                     //    we want to set these values (default is A1)
    );


    
  // Clear SESSION Variable 
  unset($_SESSION["_Arr_export_to_excel_Set_Col_header"]);
  unset($_SESSION["_Arr_export_to_excel_GET_ALL_dt_Lists"]);


// Set worksheet title
// $spreadsheet->getActiveSheet()->setTitle('Simple');
 
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$spreadsheet->setActiveSheetIndex(0);

 // save with browser
// Redirect output to a client's web browser (Xlsx)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$Set_file_name.'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
 
// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0
 
 
//old PhpExcel code:
//$writer = PHPExcel_IOFactory::createWriter($spreadsheet, 'Excel2007');
//$writer->save('php://output');
 
//new code:
$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit;
}

?>






