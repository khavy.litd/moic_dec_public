



<?php 

function Generate_PDF_Bill_order($html) { 

    require_once (ROOT_Server_Directory_Path_Project."_Extensions.Tool/TCPDF/TCPDF_Class/tcpdf.php");
    
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    
    
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Nicola Asuni');
    $pdf->SetTitle('TCPDF Example 061');
    $pdf->SetSubject('TCPDF Tutorial');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
    
    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    
    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    
    
    
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    
    /*
     // set some language-dependent strings (optional)
     if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
     require_once(dirname(__FILE__).'/lang/eng.php');
     $pdf->setLanguageArray($l);
     }
     */
    
    // ---------------------------------------------------------
    
    // set font
    $pdf->SetFont('helvetica', '', 10);
    
    // add a page
    $pdf->AddPage();
    
    
    // output the HTML content
    $pdf->writeHTML($html, true, false, true, false, '');
    
    // reset pointer to the last page
    $pdf->lastPage();
    
    // ---------------------------------------------------------
    /*
    // write some JavaScript code
    $js = <<<EOD
app.alert('JavaScript Popup Example', 3, 0, 'Welcome');
var cResponse = app.response({
    cQuestion: 'How are you today?',
    cTitle: 'Your Health Status',
    cDefault: 'Fine',
    cLabel: 'Response:'
});
if (cResponse == null) {
    app.alert('Thanks for trying anyway.', 3, 0, 'Result');
} else {
    app.alert('You responded, "'+cResponse+'", to the health question.', 3, 0, 'Result');
}
EOD;
    */
    
    
    // force print dialog
    $js .= 'print(true);';
    
    // set javascript
    $pdf->IncludeJS($js);
    
    // ---------------------------------------------------------
    
    
    // Clean any content of the output buffer
    ob_end_clean(); // TCPDF ERROR: Some data has already been output, can't send PDF file 
    // ob_end_clean(); // TCPDF ERROR: Some data has already been output, can't send PDF file 
    
    //Close and output PDF document
    $create_file_pdf = $pdf->Output("bill_order_print.pdf", 'I');
    echo file_get_contents($create_file_pdf);
    
}

?>





