
<?php 
function init_config_tinymce_Get_elements($elements) { 
?>

<script src="<?php echo GLOBAL_Path_Extensions_Tool_URL; ?>tinymce/tinymce.min.js"></script>

<script>

tinymce.init({
 // selector: 'textarea',  // change this value according to your HTML

 forced_root_block : "" , // "p"
 // Remove 'p' tags that are auto added within tinymce
 
  mode : "exact",		 
  //elements :"news_detail_en,news_detail_th,news_brief_en,news_brief_th,event_brief_en,event_brief_th,faq_answer_en,faq_answer_th,vdo_banner_en,vdo_banner_th",
  elements :"<?php echo $elements; ?>", // :"detail_en,detail_th,faq_answer_en,faq_answer_th"
  height: 300,
  width : 1024,
  extended_valid_elements: 'span',

  plugins: [
  "advlist autolink link image lists charmap print preview hr anchor pagebreak",
  "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
  "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
  ], //  

  toolbar: 'undo redo | formatselect | bold italic strikethrough forecolor backcolor permanentpen formatpainter | link unlink anchor | image media pageembed | alignleft aligncenter alignright alignjustify | numlist bullist outdent indent | removeformat | addcomment | print preview code | help | styleselect | responsivefilemanager',
  // | responsivefilemanager 

  // toolbar: "undo redo |  styleselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | responsivefilemanager | help",
  //toolbar: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",


  image_advtab: true ,

  external_filemanager_path:"<?= GLOBAL_Path_Extensions_Tool_URL ?>filemanager/",
  filemanager_title:"Responsive Filemanager" ,
  external_plugins: { "filemanager" : "<?= GLOBAL_Path_Extensions_Tool_URL ?>filemanager/plugin.min.js"}
  ,relative_urls:false,
  remove_script_host:false,
  document_base_url:""
});


</script>



<?php 
}
?>




