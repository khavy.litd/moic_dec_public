<?php   
require_once("../_config_Layout_Template/layout_template_header.php"); 
function GLOBAL_Set_body_onload() { return  "  GET_Active_Lv1_LeftSidebarMenu('_NoSelect' , ''); "; } 


require_once(ROOT_Directory_Path_class_pattern."mysql.class_pattern.php"); //  " C:/xampp/htdocs "....."/mysql.class_pattern.php"
$mysql_class_pattern = new mysql_class_pattern();

$dt = $mysql_class_pattern->SELECT_Pattern("*" , "users_admin" , "WHERE id=:id" , null , null , array(":id" => $_SESSION["users_admin+id"]) , "fetch(PDO::FETCH_ASSOC)");

?>

  <div class="form-pad">
    <h1>User Profile </h1>
    <table width="100%" cellspacing="0" cellpadding="4" border="0">
      <tbody>
        <tr>
          <td width="31%" valign="top">Name - Surname:</td>
          <td width="67%"><?php echo $dt["full_name"];?></td>
          <td width="2%" valign="top">&nbsp;</td>
        </tr>
        <tr>
          <td valign="middle">Username: </td>
          <td valign="middle"><?php echo $_SESSION["users_admin+user_name"];?></td>
          <td valign="top">&nbsp;</td>
        </tr>
        <tr>
          <td valign="top">Password:</td>
          <td valign="top">******</td>
          <td valign="top">&nbsp;</td>
        </tr>
        <tr>
          <td valign="top">Photo Cover:</td>
          <td>
          <img src="<?php echo GLOBAL_Path_main_Folder_photo."Admin/users_admin/".$dt["photo_filename"]; ?>"  onerror="this.src='<?php echo GLOBAL_Path_icon_Template_image_Admin; ?>users_admin_photo/no_photo.png';"  width="100" height="100"  alt="">


          </td>
          <td valign="top">&nbsp;</td>
        </tr>


        <tr>
          <td valign="top" align="left">User Permissions:</td>

          <td <?php echo ($_SESSION["users_admin+permission_0"]=="yes") ? "":'style="display:none;"';?>>Administrator</td>

          <td <?php echo ($_SESSION["users_admin+permission_0"]=="yes") ? 'style="display:none;"':"";?>>
            <table border="0" cellspacing="0" cellpadding="1">

              <tr <?php echo ($_SESSION["users_admin+permission_1"]=="yes" ) ? '':'style="display: none;"';?>>
                <td><img src="<?php echo GLOBAL_Path_icon_Template_image_Admin;?>icn_check.gif" width="14" height="14" />&nbsp;</td>
                <td class="field_text">Home Banner</td>
              </tr>
              <tr <?php echo ($_SESSION["users_admin+permission_2"]=="yes" ) ? '':'style="display: none;"';?>>
                <td><img src="<?php echo GLOBAL_Path_icon_Template_image_Admin;?>icn_check.gif" width="14" height="14" />&nbsp;</td>
                <td class="field_text">Promotions</td>
              </tr>
              <tr <?php echo ($_SESSION["users_admin+permission_3"]=="yes" ) ? '':'style="display: none;"';?>>
                <td><img src="<?php echo GLOBAL_Path_icon_Template_image_Admin;?>icn_check.gif" width="14" height="14" />&nbsp;</td>
                <td class="field_text">Knowledge</td>
              </tr>
              <tr <?php echo ($_SESSION["users_admin+permission_4"]=="yes" ) ? '':'style="display: none;"';?>>
                <td><img src="<?php echo GLOBAL_Path_icon_Template_image_Admin;?>icn_check.gif" width="14" height="14" />&nbsp;</td>
                <td class="field_text">FAQ</td>
              </tr>
              <tr <?php echo ($_SESSION["users_admin+permission_5"]=="yes" ) ? '':'style="display: none;"';?>>
                <td><img src="<?php echo GLOBAL_Path_icon_Template_image_Admin;?>icn_check.gif" width="14" height="14" />&nbsp;</td>
                <td class="field_text">Contact US</td>
              </tr>
              <tr <?php echo ($_SESSION["users_admin+permission_6"]=="yes" ) ? '':'style="display: none;"';?>>
                <td><img src="<?php echo GLOBAL_Path_icon_Template_image_Admin;?>icn_check.gif" width="14" height="14" />&nbsp;</td>
                <td class="field_text">News & CSR</td>
              </tr>
              <tr <?php echo ($_SESSION["users_admin+permission_7"]=="yes" ) ? '':'style="display: none;"';?>>
                <td><img src="<?php echo GLOBAL_Path_icon_Template_image_Admin;?>icn_check.gif" width="14" height="14" />&nbsp;</td>
                <td class="field_text">Careers</td>
              </tr>
              <tr <?php echo ($_SESSION["users_admin+permission_8"]=="yes" ) ? '':'style="display: none;"';?>>
                <td><img src="<?php echo GLOBAL_Path_icon_Template_image_Admin;?>icn_check.gif" width="14" height="14" />&nbsp;</td>
                <td class="field_text">Document Download</td>
              </tr>
              <tr <?php echo ($_SESSION["users_admin+permission_9"]=="yes" ) ? '':'style="display: none;"';?>>
                <td><img src="<?php echo GLOBAL_Path_icon_Template_image_Admin;?>icn_check.gif" width="14" height="14" />&nbsp;</td>
                <td class="field_text">Products</td>
              </tr>

            </table>
          </td>

          <td valign="top">&nbsp;</td>
        </tr>


        <tr>
          <td valign="top" align="left">Status:</td>
          <td><?php echo $dt["status_user"];?></td>
          <td valign="top">&nbsp;</td>
        </tr>
        <tr>
          <td valign="top" align="left">Date Modify:</td>
          <td><?php echo $dt["update_datetime"]." By ".$dt["user_name_update"];?></td>
          <td valign="top">&nbsp;</td>
        </tr>


        <tr <?php echo ($_SESSION["users_admin+permission_0"]=="yes") ? 'style="display:none;"':"";?>>
          <td valign="top" align="left">&nbsp;</td>
          <td><span style="padding-top:20px;">
            <a href="login_profile_edit.view.php"><input type="button" class="btn-submit" value="Edit Profile"></a>
          </span></td>
          <td valign="top">&nbsp;</td>
        </tr>




      </tbody>
    </table>
  </div>
<!-- <div class="clear"></div> -->







<?php   require_once("../_config_Layout_Template/layout_template_footer.php");   ?>






