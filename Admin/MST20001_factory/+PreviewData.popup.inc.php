




<!-- How to ວິທີໃຊ້ -->
<!-- <button type="button" onclick="_shared_FUNC_Call_PreviewData_popup($id)"> Launch demo modal </button> -->







<!-- Modal dialog -->
<div class="modal fade" id="_ID_PreviewData_popup" tabindex="-1" role="dialog" aria-labelledby="LongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content" style="width: 900px; margin: 1px -167px; background-color: #eeeef5;">
          <div class="modal-header">
              <h2>Preview Data</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
  <!-- Modal dialog -->
  
  
  
   <!-- <div id="Wrapper" > --> <!--  style="width: 900px; margin: 1px -167px;"> -->
    <div class="box-addData" style="margin:0; border-radius:none; background-color: #eeeef5;">
      <!-- <h2>Preview Data</h2> -->
      <div class="box-form">
  
  
      
          <div class="form-pad">
            <table width="100%" border="0" cellspacing="0" cellpadding="4" >
            <tbody>
              <tr >
                <td width="" valign="top">ປະເພດມາດຕະຖານການປ່ອຍນ້ຳຂອງໂຮງງານ : &nbsp;&nbsp;
                  <strong><span style="font-size: 22px;" id="Preview_ID: _id_Moic_dec_Library_TBB504_header_1_cate_lv1"></span></strong>
                </td>
              </tr>
              <tr >
                <td width="" valign="top">ປະເພດມາດຕະຖານການທາງອາກາດຂອງໂຮງງານ : &nbsp;&nbsp;
                  <strong><span style="font-size: 22px;" id="Preview_ID: _id_Moic_dec_Library_TBB504_header_2_cate_lv1"></span></strong>
                </td>
              </tr>
            </tbody></table>
          </div>

          <!-- style="display: none;" -->


          <fieldset>
            <legend> <img src="<?php echo GLOBAL_Path_icon_Template_image_Admin;?>icon/icon-time-and-date.png" width="32" height="32" alt="" /> ເພີ້ມຂໍ້ມູນຊື່ຂອງໂຮງງານ </legend>
            <div class="form-pad">
              <table width="100%" border="0" cellspacing="0" cellpadding="4">
                <tr>
                  <td valign="top" width="22%">ລະຫັດ ISIC:</td>
                  <td id="Preview_ID: factory_code_isic"></td>
                </tr>
                <tr>
                  <td valign="top">ເລກທີລົງທະບຽນ:</td>
                  <td id="Preview_ID: factory_registration_number"></td>
                </tr>
                <tr>
                  <td valign="top">ຊື່ໂຮງງານ(ລາວ):</td>
                  <td id="Preview_ID: factory_name_lo"></td>
                </tr>
                <tr>
                  <td valign="top" width="22%">ຊື່ໂຮງງານ(ENG):</td>
                  <td id="Preview_ID: factory_name_en"></td>
                </tr>
                <tr>
                  <td valign="top">ຊື່ປະເພດໂຮງງານ(ລາວ):</td>
                  <td id="Preview_ID: factory_cate_name_lo"></td>
                </tr>
                <tr>
                  <td valign="top">ຊື່ປະເພດໂຮງງານ(ENG):</td>
                  <td id="Preview_ID: factory_cate_name_en"></td>
                </tr>
                <tr>
                  <td valign="top" width="22%">ຊື່ເຈົ້າຂອງໂຮງງານ(ລາວ):</td>
                  <td id="Preview_ID: factory_owner_name_lo"></td>
                </tr>
                <tr>
                  <td valign="top">ຊື່ເຈົ້າຂອງໂຮງງານ(ENG):</td>
                  <td id="Preview_ID: factory_owner_name_en"></td>
                </tr>
              </table>
            </div>
          </fieldset>
          
          <fieldset>
            <legend> <img src="<?php echo GLOBAL_Path_icon_Template_image_Admin;?>icon/icn-paragraph.png" width="32" height="32" alt="" /> ເພີ້ມປະຫວັດການດໍາເນີນທຸລະກິດ </legend>
            <div class="form-pad">
              <table width="100%" border="0" cellspacing="0" cellpadding="4">
                <tr>
                  <td width="22%" valign="top">ວັນ/ເດືອນ/ປີ ດຳເນີນທຸລະກິດ</td>
                  <td width="85%" id="Preview_ID: start_production_datetime"></td>
                </tr>
                <tr>
                  <td width="22%" valign="top">ປີ ອະນຸຍາດດຳເນີນທຸລະກິດ</td>
                  <td width="85%" id="Preview_ID: start_production_year"></td>
                </tr>
                <tr>
                  <td width="22%" valign="top">ເບີໂທ:</td>
                  <td width="85%" id="Preview_ID: tel"></td>
                </tr>
                <tr>
                  <td width="22%" valign="top">Fax:</td>
                  <td width="85%" id="Preview_ID: fax"></td>
                </tr>
                <tr>
                  <td width="22%" valign="top">Email:</td>
                  <td width="85%" id="Preview_ID: email"></td>
                </tr>
                <tr>
                  <td width="22%" valign="top">ແຂວງ:</td>
                  <td width="85%" id="Preview_ID: province"></td>
                </tr>
                <tr>
                  <td width="22%" valign="top">ເມືອງ:</td>
                  <td width="85%" id="Preview_ID: district"></td>
                </tr>
                <tr>
                  <td width="22%" valign="top">ບ້ານ:</td>
                  <td width="85%" id="Preview_ID: village"></td>
                </tr>
              </table>
            </div>
          </fieldset>

          <!-- -->
          
          <fieldset style="display: block;" > <!-- style="display: none;" -->
            <legend> <img src="<?php echo GLOBAL_Path_icon_Template_image_Admin;?>icon/icn-paragraph.png" width="32" height="32" alt="" /> </legend>
            <div class="form-pad">
              <table width="100%" border="0" cellspacing="0" cellpadding="4">
                <tr>
                  <td valign="top" width="22%">Description:</td>
                  <td id="Preview_ID: description"></td>
                </tr>
              </table>
            </div>
          </fieldset>


          <fieldset id="Main_Table_detail" style="display: none;" > <!-- style="display: none;" -->
            <legend> <img src="<?php echo GLOBAL_Path_icon_Template_image_Admin;?>icon/icon-checklist.png" width="32" height="32" alt="" /> ລາຍການ Parameters </legend>
            <div class="form-pad">

            <table width="100%" class="form-pad" id="Rows_Main_Table_detail_list" style="display: block;" > <!-- style="display: none;" -->
            </table>

              <table width="100%" border="0" cellspacing="0" cellpadding="4">
                <tr>
                  <td valign="top" width="22%">Total Parameters:</td>
                  <td id="Preview_ID: Count_items_Main_Table_detail_Join_by_id_Main_Table_header"></td>
                </tr>
              </table>
            </div>
          </fieldset>


          <div class="form-pad">
            <table width="100%" border="0" cellspacing="0" cellpadding="4">
              <tr>
                <td valign="top">Date Modify: </td>
                <td id="Preview_ID: update_datetime"></td>
              </tr>
            </table>
          </div>

          <!--//All-->
         
          

  
        </div><!-- <div class="box-form"> -->
       </div> <!-- <div class="box-addData"  -->
<!-- </div>  --><!--<div id="Wrapper"  -->
          
  
  
         
        
  
  
  
      
  <!--// Modal dialog -->
          </div> <!-- // <div class="modal-body"> -->
      </div> <!-- // <div class="modal-content" -->
  
    </div><!-- <div class="modal-dialog" role="document"> -->
  </div><!-- <div class="modal fade" id="_ID_" -->
  <!--// Modal dialog -->
  
  
  
  
  
  
  
     
   
  

<script>


function _shared_FUNC_Call_PreviewData_popup(GET_id , _id_Moic_dec_Library_TBB504_header_1_cate_lv1, _id_Moic_dec_Library_TBB504_header_2_cate_lv1) { 
// AJAX_Select_Main_Table_fetch_By_id(GET_id);

  _shared_FUNC_Call_wait_loading_page('show');

    // AXIOS   
    var params = new URLSearchParams();
    params.append("AJAX_Sent+||+ROOT_Path_config_inc" , "<?php echo ROOT_Server_Directory_Path_Project."_config_Layout_Template/config.inc.php"; ?>");
    params.append("Req_Call_Func" , "FuncJsAJAX_Select_Main_Table_fetch_By_id");
    params.append("id" , GET_id); 
    axios.post("ManageData.Class.php" , params) // 
      .then(function(res) { //[ if Success ] Get Return JSON data from url .php : Data Response
         // console.log(res.data);  // console.log(res.data.sql_status);   //   console.log(res.data.message);

          var sql_status = res.data.sql_status;
          var message = res.data.message;

          var arr_fetch = res.data.arr_fetch; // console.log(arr_fetch); 

          // var arr_fetch_header = res.data.arr_fetch_header; // console.log(arr_fetch_header); 
          /*
          var arr_fetchAll_detail = res.data.arr_fetchAll_detail; // console.log(arr_fetchAll_detail); 
          var Count_items_Main_Table_detail_Join_by_id_Main_Table_header = res.data.Count_items_Main_Table_detail_Join_by_id_Main_Table_header; 
          */

          if(sql_status !== "has_row") { 
            _shared_FUNC_Call_wait_loading_page('hide'); 
            _shared_FUNC_Call_Alert_dialogBox_Message("warning", "ERROR sql_status", message); // _shared_FUNC_Call_Alert_notification("cart",  "Test" + "", 3); 
            return false;
          } 

          document.getElementById("Preview_ID: _id_Moic_dec_Library_TBB504_header_1_cate_lv1").innerHTML = __Show_AND_Convert_Data_to_Text_html(_id_Moic_dec_Library_TBB504_header_1_cate_lv1);
          document.getElementById("Preview_ID: _id_Moic_dec_Library_TBB504_header_2_cate_lv1").innerHTML = __Show_AND_Convert_Data_to_Text_html(_id_Moic_dec_Library_TBB504_header_2_cate_lv1);

          document.getElementById("Preview_ID: factory_code_isic").innerHTML = __Show_AND_Convert_Data_to_Text_html(arr_fetch['factory_code_isic']);
          document.getElementById("Preview_ID: factory_registration_number").innerHTML = __Show_AND_Convert_Data_to_Text_html(arr_fetch['factory_registration_number']);
          document.getElementById("Preview_ID: factory_name_lo").innerHTML = __Show_AND_Convert_Data_to_Text_html(arr_fetch['factory_name_lo']);
          document.getElementById("Preview_ID: factory_name_en").innerHTML = __Show_AND_Convert_Data_to_Text_html(arr_fetch['factory_name_en']);
          document.getElementById("Preview_ID: factory_cate_name_lo").innerHTML = __Show_AND_Convert_Data_to_Text_html(arr_fetch['factory_cate_name_lo']);
          document.getElementById("Preview_ID: factory_cate_name_en").innerHTML = __Show_AND_Convert_Data_to_Text_html(arr_fetch['factory_cate_name_en']);
          document.getElementById("Preview_ID: factory_owner_name_lo").innerHTML = __Show_AND_Convert_Data_to_Text_html(arr_fetch['factory_owner_name_lo']);
          document.getElementById("Preview_ID: factory_owner_name_en").innerHTML = __Show_AND_Convert_Data_to_Text_html(arr_fetch['factory_owner_name_en']);
          document.getElementById("Preview_ID: start_production_datetime").innerHTML = __Show_AND_Convert_Data_to_Text_html(arr_fetch['start_production_datetime']);
          document.getElementById("Preview_ID: start_production_year").innerHTML = __Show_AND_Convert_Data_to_Text_html(arr_fetch['start_production_year']);
          document.getElementById("Preview_ID: tel").innerHTML = __Show_AND_Convert_Data_to_Text_html(arr_fetch['tel']);
          document.getElementById("Preview_ID: fax").innerHTML = __Show_AND_Convert_Data_to_Text_html(arr_fetch['fax']);
          document.getElementById("Preview_ID: email").innerHTML = __Show_AND_Convert_Data_to_Text_html(arr_fetch['email']);
          document.getElementById("Preview_ID: province").innerHTML = __Show_AND_Convert_Data_to_Text_html(arr_fetch['province']);
          document.getElementById("Preview_ID: district").innerHTML = __Show_AND_Convert_Data_to_Text_html(arr_fetch['district']);
          document.getElementById("Preview_ID: village").innerHTML = __Show_AND_Convert_Data_to_Text_html(arr_fetch['village']);
          // document.getElementById("Preview_ID: photo_filename").innerHTML = __Show_AND_Convert_Data_to_Text_html(arr_fetch['photo_filename'],"img");
          document.getElementById("Preview_ID: description").innerHTML = __Show_AND_Convert_Data_to_Text_html(arr_fetch['description']);
          document.getElementById("Preview_ID: update_datetime").innerHTML = arr_fetch['update_datetime'] + " By " + arr_fetch['user_name_update'];

/* 

          document.getElementById("Preview_ID: Count_items_Main_Table_detail_Join_by_id_Main_Table_header").innerHTML = "[ "+Count_items_Main_Table_detail_Join_by_id_Main_Table_header +" ] Parameters";

        // +++++++++++ "Main_Table_detail" ++++++++++++++++++++ 
    
          if(arr_fetchAll_detail === null) {document.getElementById("Main_Table_detail").style.display = "none";} else { document.getElementById("Main_Table_detail").style.display = "block"; }

          var table = document.getElementById("Rows_Main_Table_detail_list");

          for(var key in arr_fetchAll_detail) { 
            document.getElementById("Main_Table_detail").style.display = "block";
            var ARR_Rows_dtl = arr_fetchAll_detail[key];
            // console.log(ARR_Rows_dtl.id); console.log(ARR_Rows_dtl['parameter_name_lo']);
            
            var row = table.insertRow(0);
            var cell1 = row.insertCell(0);  var cell2 = row.insertCell(1); var cell3 = row.insertCell(2); var cell4 = row.insertCell(3); var cell5 = row.insertCell(4);
            cell1.innerHTML = ARR_Rows_dtl['parameter_name_lo'];  cell2.innerHTML = ARR_Rows_dtl['std_consumption_rate_limited_parameter_min']; cell3.innerHTML = ARR_Rows_dtl['std_consumption_rate_limited_parameter_max'];  cell4.innerHTML = ARR_Rows_dtl['consumed_quantity_parameter']; cell5.innerHTML = ARR_Rows_dtl['unit_name_lo']; 
          }

          var row = table.insertRow(0);
          var cell1 = row.insertCell(0);  var cell2 = row.insertCell(1); var cell3 = row.insertCell(2); var cell4 = row.insertCell(3); var cell5 = row.insertCell(4);
          cell1.innerHTML = "Parameters";  cell2.innerHTML = "ຄ່າກຳນົດ Min"; cell3.innerHTML = "ຄ່າກຳນົດ MAX";  cell4.innerHTML = "ຄ່າການນຳໃຊ້"; cell5.innerHTML = "ໜ່ວຍວັດແທກ"; 

          document.getElementById("Rows_Main_Table_detail_list").setAttribute("border", "1");
          document.getElementById("Rows_Main_Table_detail_list").setAttribute("width", "100%");
          document.getElementById("Rows_Main_Table_detail_list").setAttribute("align", "center");

        // END: +++++++++++ "Main_Table_detail" ++++++++++++++++++++ 


*/


          _shared_FUNC_Call_wait_loading_page('hide');
          $("#_ID_PreviewData_popup").modal('show'); // toggle 

      }).catch(function (error) { _shared_FUNC_Call_Alert_dialogBox_Message("warning", "ERROR try Catch", error); console.log(error);  });

}



function __Show_AND_Convert_Data_to_Text_html(GET_value , type=null) {
  const Show_txthtml_No_Data = '<span class="txt-red">No data!</span>';
  if(type == null) {
    if(GET_value == null) { return Show_txthtml_No_Data; } // ປ້ອງກັນ TypeError cannot read property 'trim' of null
    return (GET_value.trim() === "" ? Show_txthtml_No_Data : GET_value);
  }

  /*
  if(type == "img") { 
    if(GET_value == null) { return ""; } // ປ້ອງກັນ TypeError cannot read property 'trim' of null
    return (GET_value.trim() === "" ? "" : '<img src="<?php // echo _SET_Path_Folder_Photo_full_Size;?>'+GET_value+'" style="width: 700px" />');
  }
  */

  return GET_value;
  
}

</script>





